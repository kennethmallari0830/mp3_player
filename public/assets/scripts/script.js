"use strict";

// Select Elements from the DOM:
const musicContainer = document.querySelector("#music-container");

const playBtn = document.querySelector("#play");
const prevBtn = document.querySelector("#prev");
const nextBtn = document.querySelector("#next");

const audio = document.querySelector("audio");
const progress = document.querySelector("#progress");
const progressContainer = document.querySelector("#progress-container");
const title = document.querySelector("#title");
const cover = document.querySelector("#cover");
const backgroundColorEl = document.querySelector("#background");

//Song Titles:
const songs = ["Battle_Belongs", "House_of_the_Lord", "You_Cannot_Be_Stopped"];

//Track songs:
let songIndex = 0;

//Function to update song details:
const loadSong = function (song) {
  title.innerText = song;
  audio.src = `./assets/music/${song}.mp3`;
  cover.src = `./assets/images/${song}.jpg`;
  backgroundColor();
};

//Function to play song
const playSong = function () {
  musicContainer.classList.add("play");
  playBtn.querySelector("i.fas").classList.remove("fa-play");
  playBtn.querySelector("i.fas").classList.add("fa-pause");
  audio.play();
};

//Function to pause song:

const pauseSong = function () {
  musicContainer.classList.remove("play");
  playBtn.querySelector("i.fas").classList.remove("fa-pause");
  playBtn.querySelector("i.fas").classList.add("fa-play");
  audio.pause();
};

//Function for Previous song:
const prevSong = function () {
  songIndex--;
  console.log(songIndex);
  if (songIndex < 0) {
    songIndex = songs.length - 1;
  }
  console.log(songIndex);
  loadSong(songs[songIndex]);
  playSong();
};

//Function for Next song:
const nextSong = function () {
  songIndex++;
  if (songIndex > songs.length - 1) {
    songIndex = 0;
  }

  loadSong(songs[songIndex]);
  playSong();
};

// Function for updating songs time progress
const updateProgress = function (e) {
  const { duration, currentTime } = e.srcElement;
  const progressPercentage = (currentTime / duration) * 100;
  progress.style.width = `${progressPercentage}%`;
};

const setProgress = function (e) {
  const width = progressContainer.clientWidth;
  const clickX = e.offsetX;
  const duration = audio.duration;

  audio.currentTime = (clickX / width) * duration;
  console.log(audio.currentTime);
};

const backgroundColor = function () {
  const r = (Math.random() * 256) >> 0;
  const color = `rgba(${r},${r},${r},${0.4})`;
  backgroundColorEl.style.backgroundColor = color;
};

//Initially load song details into DOM:
loadSong(songs[songIndex]);

//***************************************Event Listeners***************************************:

playBtn.addEventListener("click", function () {
  const isPlaying = musicContainer.classList.contains("play");
  if (isPlaying) {
    pauseSong();
  } else {
    playSong();
  }
});

//Change song

prevBtn.addEventListener("click", prevSong);
nextBtn.addEventListener("click", nextSong);

//Time and song update:
audio.addEventListener("timeupdate", updateProgress);

//Clicks on the progress bar
progressContainer.addEventListener("click", setProgress);

//Event when song ends
audio.addEventListener("ended", nextSong);
